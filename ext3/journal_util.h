#ifndef __JOURNAL_UTIL_H
#define __JOURNAL_UTIL_H

#include "fs.h"
#include "inode.h"
#include <journal.h>
#include <assert.h>

/**
 * Open a transaction.
 * @param num Maximum number of blocks needed to modify.
 */

#define journal_begin(num) \
      {\
        if (h != NULL) {\
            panic("ext3: h must be NULL on begin()\n"); \
        }\
        if ((h = journal_start(jp, (num))) == NULL) \
        { \
            printf("ext3: journal_start() failed: %s\n", strerror(errno)); \
            return(EINVAL); \
        } \
       }	

/**
 * End current transaction.
 */
#define journal_end() \
    {\
        /*assert(h != NULL);*/ \
        if (h == NULL) {\
            panic("ext3: h must not be NULL on end()\n"); \
        }\
        if (journal_stop(h) != 0) \
        { \
            printf("ext3: journal_stop() failed: %s\n", strerror(errno)); \
            h = NULL; \
            return(EINVAL); \
        } \
        h = NULL; \
    }

_PROTOTYPE(PUBLIC int ext3_init_journal, (dev_t fs_dev, ino_t inode));
_PROTOTYPE(PUBLIC int ext3_deinit_journal, (dev_t fs_dev));

EXTERN journal_t *jp;
EXTERN handle_t *h;

#endif /* __JOURNAL_UTIL_H */



