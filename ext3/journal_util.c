#include "fs.h"
#include "buf.h"
#include "inode.h"
#include "drivers.h"
#include "super.h"
#include <string.h>
#include "journal_util.h"

FORWARD _PROTOTYPE(journal_block_t * ext3_read_block, (void *ctx, block_t nr, journal_t *jp));
FORWARD _PROTOTYPE(int ext3_write_block, (void *ctx, journal_block_t *jb, journal_t *jp));
FORWARD _PROTOTYPE(int ext3_writeall_block, (void *ctx, journal_block_list_t *list, journal_t *jp));
FORWARD _PROTOTYPE(journal_block_t *ext3_read_cache, (void *ctx, block_t blocknr, journal_t *jp, int noread));
FORWARD _PROTOTYPE(int ext3_write_cache, (void *ctx, journal_block_t *block, journal_t *jp));
FORWARD _PROTOTYPE(journal_block_t * ext3_read_log, (void *ctx, block_t nr, journal_t *jp));
FORWARD _PROTOTYPE(int ext3_write_log, (void *ctx, journal_block_t *jb, journal_t *jp));
FORWARD _PROTOTYPE(int ext3_writeall_log, (void *ctx, journal_block_list_t *list, journal_t *jp));
FORWARD _PROTOTYPE(int ext3_flush, (void *ctx));
FORWARD _PROTOTYPE(void * ext3_alloc_contig, (size_t sz));
FORWARD _PROTOTYPE(void ext3_free_contig, (void *ptr, size_t sz));
FORWARD _PROTOTYPE(int ext3_do_scatter, (void *ctx, journal_block_list_t *list, journal_t *jp, int do_mapping));
/* FORWARD _PROTOTYPE(void ext3_do_mark_orphan, (int slot, ino_t)); */


PRIVATE struct inode *journal_i;
PRIVATE block_t *journal_bmap;
PRIVATE journal_io_t jdev, fsdev;
PUBLIC journal_t *jp = NULL;
PUBLIC handle_t *h = NULL;


PUBLIC int ext3_init_journal(dev_t fs_dev, ino_t inode)
{
	int i;
	size_t nblocks;
	struct super_block *sp;
	u8_t blocksize_bits;
	u16_t inode_size;
      printf("inside ext3_init_journal\n");
      
      
	/* Get the journal log inode */
	printf("inode number being requested is - %d\n", inode);
	if ((journal_i = get_inode(fs_dev, inode)) == NULL)
	{
		printf("ext3: Failed to get journal inode\n");
		return -1;
	}
	printf("Getting journal inode is successful\n");

	printf("Current mode - %08x\n", journal_i->i_mode);
	printf("mode - %08x\n", I_REGULAR);

	/* Verify its mode */
	if ((journal_i->i_mode & 00170000) != I_REGULAR ||
		journal_i->i_uid  !=         0 ||
		journal_i->i_gid    !=         0)
	{
		printf("mode=%d uid=%d gid=%d\n", journal_i->i_mode,journal_i->i_uid, journal_i->i_gid);
		printf("ext3: Invalid mode for a journal inode\n");
		put_inode(journal_i);
		return -1;
	}
	printf("journal inode mode verified\n");

        /* Find the max length of the journal from its inode */
        nblocks = journal_i->i_size >> journal_i->i_sp->s_blocksize_bits;
	printf("numberof journal blocks - %d\n", nblocks);

	/* Allocate block map table */
        if ( !(journal_bmap = (block_t *) malloc(nblocks * sizeof(block_t))) )
        {
            printf("ext3: failed to malloc() journal_bmap: %s\n",
                strerror(errno));
            put_inode(journal_i);
            return -1;
        }

	/* Fill it with block mappings of journal_i */
	for (i = 0; i < nblocks; i++)
	{
		journal_bmap[i] = read_map(journal_i, i * fs_block_size);

		if (journal_bmap[i] == NO_BLOCK)
		{
			printf("ext3: Failed to init journal_bmap[%d]=%d, nblk=%d, isz=%d, blksz=%d\n",
				   i, i * fs_block_size, nblocks, journal_i->i_size,
				   fs_block_size);
			/* put_block(journal_i, NORMAL); */
			return -1;
		}
	}

	/* Initialize I/O callbacks for libjournal */
	memset(&jdev, 0, sizeof(jdev));
	memset(&fsdev, 0, sizeof(fsdev));

	/* #TODO: implement callbacks */
	jdev.read_block = ext3_read_log;
	jdev.write_block = ext3_write_log;
	jdev.write_all = ext3_writeall_log;
	jdev.malloc = ext3_alloc_contig;
	jdev.free = ext3_free_contig;

	/* #TODO: implement callbacks */
	fsdev.read_cache = ext3_read_cache;
	fsdev.write_cache = ext3_write_cache;
	fsdev.read_block = ext3_read_block;
	fsdev.write_block = ext3_write_block;
	fsdev.write_all = ext3_writeall_block;
	fsdev.flush = ext3_flush;

	printf("Going to initialize journal\n");
	/* Initialize libjournal */
	printf("fs_block_size - %d, nblocks - %d\n", fs_block_size,nblocks);
	if ( !(jp = journal_init(
			&jdev, &fsdev, 0, nblocks, fs_block_size)) )
	{
		printf("ext3: journal_init() failed: %s\n", strerror(errno));
		put_inode(journal_i);
		return -1;
	}

	/* Attempt to load the journal */
	printf("Just before journal_load\n");
	if (journal_load(jp) != 0)
	{
		printf("ext3: journal_load() failed: %s\n", strerror(errno));
		put_inode(journal_i);
		return -1;
	}
	printf("Just after journal_load\n");

	/* Release the journal inode */
	put_inode(journal_i);

	/* Attempt orphan recovery */
	/*ext3_recover_orphans(); */
	return 0;
}

PRIVATE journal_block_t * ext3_read_log(void *ctx, block_t nr, journal_t *jp)
{
	journal_block_t *jb;
	dev_t dev = journal_i->i_dev;
	u64_t pos;

  printf("[+] Inside ext3_read_log\n");
	/* Allocate block buffer. */
	if ( !(jb = journal_new_block(jp, nr)) )
	{
		return NULL;
	}

	/* Calculate position */
	pos = mul64u(journal_bmap[nr], fs_block_size);

	/* Perform read */
	if (block_dev_io(MFS_DEV_READ, dev, SELF_E, jb->data,
					 pos, fs_block_size) < 0)
	{
		printf("ext3:(%d) I/O error on device %d/%d, block %lu\n",
			   SELF_E, major(dev), minor(dev), jb->blocknr);
		return NULL;
	}

	else
		return jb;
}

PRIVATE int ext3_write_log(void *ctx, journal_block_t *jb, journal_t *jp)
{
	dev_t dev = journal_i->i_dev;
	u64_t pos;

  printf("[+] Inside ext3_write_log\n");
	/* Calculate position */
	pos = mul64u(journal_bmap[jb->blocknr], fs_block_size);

	/* Perform write */
	if (block_dev_io(MFS_DEV_WRITE, dev, SELF_E, jb->data,
					 pos, fs_block_size) < 0)
	{
		printf("ext3:(%d) I/O error on device %d/%d, block %lu\n",
				SELF_E, major(dev), minor(dev), jb->blocknr);
		return -1;
	}
	return 0;
}

PRIVATE int ext3_writeall_log(void *ctx, journal_block_list_t *list, journal_t *jp)
{
  printf("[+] Inside ext3_writeall_log\n");
	return ext3_do_scatter(ctx, list, jp, 1);
}

PRIVATE void * ext3_alloc_contig(size_t sz)
{
  printf("[+] Inside ext3_alloc_contig\n");
    return alloc_contig(sz, 0, NULL);
}

PRIVATE void ext3_free_contig(void *ptr, size_t sz)
{
  printf("[+] Inside ext3_free_contig\n");
    free_contig(ptr, sz);
}

PRIVATE journal_block_t * ext3_read_cache(void *ctx, block_t blocknr,
                                          journal_t *jp, int noread)
{
    journal_block_t *jb;
    struct buf *bp;

  printf("[+] Inside ext3_read_cache\n");
    /* Retrieved cached version. */
    if (!(bp = get_block(journal_i->i_dev, blocknr,
        (noread) ? NO_READ : NORMAL)))
    {
		return NULL;
    }
    /* Allocate block buffer. */
    if (!(jb = journal_from_block(jp, blocknr, bp->bp)))
    {
        return NULL;
    }
    jb->context = bp;
    printf("[+] About to return from ext3_read_cache\n");
    return jb;
}

PRIVATE int ext3_write_cache(void *ctx, journal_block_t *jb,
                             journal_t *jp)
{
    struct buf *bp = (struct buf *) jb->context;

  printf("[+] Inside ext3_write_cache\n");
    /* Release back to the cache. */
    put_block(bp, NORMAL);

    /* Cleanup journal_block_t. */
    journal_free_block(jp, jb);
    return 0;
}

PRIVATE int ext3_write_block(void *ctx, journal_block_t *jb,
                             journal_t *jp)
{
    dev_t dev = journal_i->i_dev;
    u64_t pos;

  printf("[+] Inside ext3_write_block\n");
    /* Calculate position. */
    pos = mul64u(jb->blocknr, fs_block_size);

    /* Perform write. */
    if (block_dev_io(MFS_DEV_WRITE, dev, SELF_E, jb->data,
		     pos, fs_block_size) < 0)
    {
		printf("ext3:(%d) I/O error on device %d/%d, block %lu\n",
               SELF_E, major(dev), minor(dev), jb->blocknr);
		return -1;
    }
    else
		return 0;
}

PRIVATE int ext3_writeall_block(void *ctx, journal_block_list_t *list,
                                journal_t *jp)
{
  printf("[+] Inside ext3_writeall_block\n");
    return ext3_do_scatter(ctx, list, jp, 0);
}

PRIVATE int ext3_flush(void *ctx)
{
  printf("[+] Inside ext3_flush\n");
    flushall(fs_dev);
    return 0;
}

PRIVATE int ext3_do_scatter(void *ctx, journal_block_list_t *list,
							journal_t *jp, int do_mapping)
{
	/* Write scattered data to the journal log */
	register iovec_t *iop;
	static iovec_t *iovec = NULL;
	int num, r, i;
	journal_block_t *b, *bsmall, *bfirst, *btmp;
	journal_block_list_t sorted;

  printf("[+] Inside ext3_do_scatter\n");
	STATICINIT(iovec, NR_IOREQS);
	TAILQ_INIT(&sorted);

	/* Substitute with inode blocks */
	if ( do_mapping )
	{
		TAILQ_FOREACH(b, list, b_next)
		{
			b->blocknr = journal_bmap[b->blocknr];
		}
	}
	
	/* Sort all input blocks */
	while (!TAILQ_EMPTY(list))
	{
		bsmall = NULL;
		
		TAILQ_FOREACH(b, list, b_next)
		{
			if (!bsmall || b->blocknr < bsmall->blocknr)
			{
				bsmall = b;
			}
		}
		TAILQ_REMOVE(list, bsmall, b_next);
		TAILQ_INSERT_TAIL(&sorted, bsmall, b_next);
	}
	
	/* Set up I/O vector and do I/O.  The result of dev_io is OK if everything
	 * went fine, otherwise the error code for the first failed transfer.
	 */
	while (!TAILQ_EMPTY(&sorted))
    {
		iop = iovec;
		num = 0;
		bfirst = NULL;

		/* Form I/O vector with contigeous blocks. */
		TAILQ_FOREACH_SAFE(b, &sorted, b_next, btmp)
		{
			if (bfirst == NULL)
			{
				bfirst = b;
			}
		
			else if (bfirst->blocknr + num != b->blocknr || num >= NR_IOREQS)
			{
				break;
			}
		
			iop->iov_addr = (vir_bytes) b->data;
			iop->iov_size = (vir_bytes) fs_block_size;
			iop++; num++;

			TAILQ_REMOVE(&sorted, b, b_next);
			TAILQ_INSERT_TAIL(list, b, b_next);
		}
        
		r = block_dev_io(MFS_DEV_SCATTER, fs_dev, SELF_E, iovec,
                mul64u(bfirst->blocknr, fs_block_size), num);

        /* Harvest the results.  Dev_io reports the first error it may have
         * encountered, but we only care if it's the first block that failed.
         */
        for (i = 0, iop = iovec; i < num; i++, iop++)
        {
            if (iop->iov_size != 0)
            {
                /* Transfer failed. An error? Do we care? */
                if (r != OK && i == 0)
                {
                    panic("ext3: I/O error on device %d/%d\n",
                            major(fs_dev), minor(fs_dev));
                }
            }
        }
    }
    /* Success. */
    return 0;
}
/*
PUBLIC int ext3_recover_orphans(void)
{
	int i, index = 0, offset;
	struct inode inode;
	struct buf *bp = NULL;
	struct super_block *sup = get_super(fs_dev);
	ino_t inum;


	
			inode.i_dev    = fs_dev;
			inode.i_num    = inum;
			inode.i_count  = 0;
			rw_inode(&inode, READING);
			inode.i_update = 0;
			inode.i_zsearch = NO_ZONE;
			inode.i_mountpoint = FALSE;
			
			truncate_inode(&inode, (off_t) 0);
			inode.i_mode = I_NOT_ALLOC;
			free_inode(&inode, i);
	
	return 0;
}
*/

PUBLIC int ext3_deinit_journal(dev_t fs_dev)
{
  int ret = 0;

  printf("[+] Inside ext3_deinit_journal\n");

  if ( journal_destroy(jp) != 0 )
  {
    printf("ext3: Journal destroy failed: %s\n", strerror(errno));
    ret = -1;
  }
  jp = NULL;
  return ret;
}

    
PRIVATE journal_block_t * ext3_read_block(void *ctx, block_t nr, journal_t *jp)
{
  printf("[+] Inside ext3_read_block\n");
  return NULL;
 } 
  
